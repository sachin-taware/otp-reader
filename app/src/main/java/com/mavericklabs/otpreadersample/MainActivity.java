package com.mavericklabs.otpreadersample;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mavericklabs.otp_reader.OtpReader;
import com.mavericklabs.otp_reader.OtpReaderListener;

public class MainActivity extends AppCompatActivity implements OtpReaderListener {
    private OtpReader otpReader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        otpReader = new OtpReader(this, this);
        otpReader.start();
    }

    @Override
    public void onOtpReceived(String otp) {
        Toast.makeText(this, "Otp Received " + otp, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onOtpTimeout() {
        Toast.makeText(this, "Time out, please resend", Toast.LENGTH_LONG).show();
    }
}
