package com.mavericklabs.otpreadersample;

import android.app.Application;

/**
 * Created by Prashant Kashetti on 10/10/19.
 */
public class SmsVerificationApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AppSignatureHelper appSignatureHelper = new AppSignatureHelper(this);
        appSignatureHelper.getAppSignatures();
    }
}

