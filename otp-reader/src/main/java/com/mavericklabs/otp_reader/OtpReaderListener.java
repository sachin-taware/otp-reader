package com.mavericklabs.otp_reader;

public interface OtpReaderListener {
    void onOtpReceived(String otp);

    void onOtpTimeout();
}
