package com.mavericklabs.otp_reader;

import android.content.Context;
import android.content.IntentFilter;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

/**
 * Created by Prashant Kashetti on 10/10/19.
 */
public class OtpReader {
    private SmsBroadcastReceiver mSmsBroadcastReceiver;
    private Context context;
    private OtpReaderListener otpReaderListener;

    public OtpReader(Context context, OtpReaderListener otpReaderListener) {
        this.context = context;
        this.otpReaderListener = otpReaderListener;
    }

    public void start() {
        mSmsBroadcastReceiver = new SmsBroadcastReceiver();
        mSmsBroadcastReceiver.setOnOtpListeners(otpReaderListener);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
        context.registerReceiver(mSmsBroadcastReceiver, intentFilter);
        SmsRetrieverClient mClient = SmsRetriever.getClient(context);
        Task<Void> mTask = mClient.startSmsRetriever();
        mTask.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        });
        mTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }

}
